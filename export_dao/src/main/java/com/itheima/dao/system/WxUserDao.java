package com.itheima.dao.system;

import com.itheima.domain.system.WxUser;
import com.itheima.domain.system.WxUserExample;

import java.util.List;

public interface WxUserDao {
    int deleteByPrimaryKey(String id);

    int insert(WxUser record);

    int insertSelective(WxUser record);

    List<WxUser> selectByExample(WxUserExample example);

    WxUser selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(WxUser record);

    int updateByPrimaryKey(WxUser record);
}