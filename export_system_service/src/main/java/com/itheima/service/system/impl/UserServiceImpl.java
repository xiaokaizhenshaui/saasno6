package com.itheima.service.system.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.itheima.dao.system.ModuleDao;
import com.itheima.dao.system.UserDao;
import com.itheima.dao.system.WxUserDao;
import com.itheima.domain.system.Module;
import com.itheima.domain.system.User;
import com.itheima.domain.system.WxUser;
import com.itheima.domain.system.WxUserExample;
import com.itheima.service.system.UserService;
import com.itheima.utils.HttpUtils;
import org.apache.commons.beanutils.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import com.alibaba.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Value;

import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.Map;
import java.util.UUID;

@Service
public class UserServiceImpl implements UserService {
    @Value("${wx.appid}")
    private String appid;
    @Value("${wx.secret}")
    private String secret;
    // 获取token的微信链接
    @Value("${wx.accessTokenUrl}")
    private String accessTokenUrl;
    // 获取微信人登录信息的链接
    @Value("${wx.wxInfoUrl}")
    private String wxInfoUrl;

    @Autowired
    private WxUserDao wxUserDao;
    @Autowired
    private UserDao userDao;

    @Autowired
    private ModuleDao moduleDao;

    @Override
    public List<User> findAll(String companyId) {
        return userDao.findAll(companyId);
    }

    @Override
    public void save(User user) {
        userDao.save(user);
    }

    @Override
    public User findById(String id) {
        return userDao.findById(id);
    }

    @Override
    public void update(User user) {
        userDao.update(user);
    }

    @Override
    public void delete(String id) {
        userDao.delete(id);
    }

    @Override
    public PageInfo<User> findByPage(String companyId, Integer pageNum, Integer pageSize) {
        PageHelper.startPage(pageNum, pageSize);
        List<User> list = userDao.findAll(companyId);
        return new PageInfo<>(list, 10);
    }

    @Override
    public List<String> findRoleIdsByUserId(String id) {
        return userDao.findRoleIdsByUserId(id);
    }

    @Override
    public void changeRole(String userId, String[] roleIds) {
        //1. 删除中间表中用户的现有角色对应的id
        userDao.deleteUserRoleByUserId(userId);

        //2. 重新向中间表插入新的用户和角色id
        if (roleIds != null && roleIds.length > 0) {
            for (String roleId : roleIds) {
                userDao.saveUserRole(userId, roleId);
            }
        }
    }

    @Override
    public User findByEmail(String email) {
        return userDao.findByEmail(email);
    }

    @Override
    public List<Module> findModuleByUser(User user) {
        //登录之后，根据登录的用户信息确定身份, 根据身份确定权限
        Integer degree = user.getDegree();
        if (degree == 0) {//saas管理员
            return moduleDao.findByBelong(degree);
        } else if (degree == 1) {//企业管理员
            return moduleDao.findByBelong(degree);
        } else {//企业普通员工
            return moduleDao.findByUserId(user.getId());
        }
    }

    // 通过微信code登录
    @Override
    public WxUser wxLogin(String code) throws  IllegalAccessException, InvocationTargetException {
        WxUser wxUser = null;
        //1.根据code获取access_token和openId
        String atUtl = accessTokenUrl + "?code=" + code + "&appid=" + appid + "&secret=" + secret + "&grant_type=authorization_code";
        System.out.println(atUtl);
        Map<String, Object> map1 = HttpUtils.sendGet(atUtl);
        Object access_token = map1.get("access_token");
        Object openid = map1.get("openid").toString();
        if (access_token == null && openid == null) {
            return wxUser;
        }

        //2.根据openId判断微信用户是否存在
        WxUserExample wxUserExample = new WxUserExample();
        wxUserExample.createCriteria().andOpenidEqualTo(openid.toString());
        List<WxUser> wxUserList = wxUserDao.selectByExample(wxUserExample);
        if (wxUserList.size() != 0) {
            System.out.println("如果微信用户存在返回数据库中的微信用户对象");
            //3.如果用户存在返回用户信息
            wxUser = wxUserList.get(0);
            return wxUser;
        } else {
            System.out.println("如果微信用户不存在调用微信wxInfoUrl去拿到微信用户的相关信息存储在微信用户对象中,保存到数据库");
            //4.如果用户不存在，根据access_token和openId获取微信用户信息
            String wxurl = wxInfoUrl + "?access_token=" + access_token.toString() + "&openid=" + openid.toString();
            Map<String, Object> map2 = HttpUtils.sendGet(wxurl);

            map2.remove("language");// 拿到的键多了一个,我选择删除

            wxUser = new WxUser();
            BeanUtils.populate(wxUser, map2); // 使用org.apache.commons的工具类进行object--->wxUser
            wxUser.setId(UUID.randomUUID().toString().replace("-", ""));
            wxUserDao.insertSelective(wxUser);
            System.out.println("存储成功");
        }
        return wxUser;
    }

    // 微信绑定用户--->将用户表id存进微信用户表中的user_id
    @Override
    public void userBindWeChat(String id, String userId) {
        WxUser wxUser = new WxUser();
        wxUser.setId(id);
        wxUser.setUserid(userId);
        wxUserDao.updateByPrimaryKeySelective(wxUser);
    }

    @Override
    public List<User> findUserLikeBirthday(String birthday) {
        return userDao.findUserLikeBirthday(birthday);
    }

    // 用户解绑微信
    @Override
    public int unbind(String userId) {
        // 1跟据关联的用户id查询到微信用户表的id
        WxUserExample wxUserExample = new WxUserExample();
        wxUserExample.createCriteria().andUseridEqualTo(userId);
        List<WxUser> wxUserList = wxUserDao.selectByExample(wxUserExample);

        if (wxUserList.size() == 0) {
            // 说明该用户暂未绑定微信!
            return 0;
        }
        // 用户解绑微信
        WxUser wxUser = wxUserList.get(0);
        wxUserDao.deleteByPrimaryKey(wxUser.getId());
        return 1;
    }
}
